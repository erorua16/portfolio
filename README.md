# Portfolio

## OBJECTIVES
1. Create a portfolio on your work
2. Use a wireframe as a base
3. Make a model from the wireframe
4. Use the model to create your site
5. Only use CSS and HTML
6. Use the correct tags in HTML
7. Implement Font Awesome

## TO   DO 
1. Create HTML Structure 
---- implemented rough frame
---- finished structure
2. Create CSS Structure 
---- implemented rough frame
---- created variables
---- finished structure
3. Add font Awesome library 
---- implemented awesome font library
4. Header
---- started header
---- finished header
5. Main
---- started main
---- finished main
6. Footer
---- started footer
---- finished footer
7. Nav bar
---- started nav bar
---- finished nav bar
8. Animation
---- added animations